# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render

from django.http import HttpResponseRedirect, HttpResponse, Http404
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
class stemmerAPI(APIView):
	def get(self, request):
		response = ""
		term = request.GET.get('term')
		if term:
			response = stemming_word(term)
		return HttpResponse(mark_safe(response))

def stemming_word(text):
	factory = StemmerFactory()
	stemmer = factory.create_stemmer()
	response = stemmer.stem(str(text))
	return response
